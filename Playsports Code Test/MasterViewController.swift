//
//  MasterViewController.swift
//  Playsports Code Test
//
//  Created by Stephen Wilson on 02/05/2019.
//  Copyright © 2019 Stephen Wilson. All rights reserved.
//

import UIKit
import SDWebImage

class MasterViewController: UITableViewController {

    var detailViewController: DetailViewController? = nil
    var objects = [[String:Any]]()
    let api = YouTubeAPI()


    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        if let split = splitViewController {
            let controllers = split.viewControllers
            detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
        }
        
        fetchVideos()
    }

    override func viewWillAppear(_ animated: Bool) {
        clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
        super.viewWillAppear(animated)
    }

    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let indexPath = tableView.indexPathForSelectedRow {
                let object = objects[indexPath.row]
                let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController
                controller.detailItem = object
                controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
                controller.navigationItem.leftItemsSupplementBackButton = true
            }
        }
    }

    // MARK: - Table View

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objects.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

        let object: [String:Any] = objects[indexPath.row]
        
        cell.textLabel!.text = (object["snippet"] as? [String:Any])?["title"] as? String
        
        guard let snippet: [String:Any] = object["snippet"] as? [String:Any],
            let thumbnails: [String:Any] = snippet["thumbnails"] as? [String:Any],
            let imageDict: [String:Any] = thumbnails["default"] as? [String:Any],
            let imageURLString = imageDict["url"] as? String,
            let imageURL = URL(string: imageURLString) else {
            return cell
        }
        
        SDWebImageManager.shared.loadImage(with: imageURL, options: .continueInBackground, progress: nil) { (image, data, error, cacheType, finished, url) in
            guard cell.textLabel?.text == (object["snippet"] as? [String:Any])?["title"] as? String else {
                return
            }
            
            cell.imageView?.image = image
            cell.layoutSubviews()
        }
        
        return cell
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return false
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            objects.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
        }
    }

    // MARK: YoutTube data loading
    
    func fetchVideos() {
        api.getGMBNVideos { (results, error) in
            guard results != nil else {
                return
            }
            
            guard var videoDicts = results!["items"] as? [[String:Any]] else {
                return
            }
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            dateFormatter.locale = NSLocale.current
            
            videoDicts.sort(by: { (videoA, videoB) -> Bool in
                guard let snippetA = videoA["snippet"] as? [String:Any], let snippetB = videoB["snippet"] as? [String:Any],
                    let publishedAString = snippetA["publishedAt"] as? String, let publishedA = dateFormatter.date(from: publishedAString),
                    let publishedBString = snippetB["publishedAt"] as? String, let publishedB = dateFormatter.date(from: publishedBString) else {
                        return false
                }
                
                return publishedA.timeIntervalSince1970 > publishedB.timeIntervalSince1970
            })
            
            DispatchQueue.main.async {
                self.objects = videoDicts
                self.tableView.reloadData()
            }
        }
    }
}

