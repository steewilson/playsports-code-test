//
//  YouTubeAPI.swift
//  Playsports Code Test
//
//  Created by Stephen Wilson on 02/05/2019.
//  Copyright © 2019 Stephen Wilson. All rights reserved.
//

import UIKit

typealias YouTubeAPIResultHandler = (_ results: [String:Any]?, _ error: Error?) -> Void

class YouTubeAPI: NSObject {
    
    enum SWAPIError: Error {
        case badURL
        case badDataResponse
    }
    
    let urlSession = URLSession(configuration: .default)

    func getGMBNVideos(completion: @escaping YouTubeAPIResultHandler) {
        let url = URL(string: "https://www.googleapis.com/youtube/v3/playlistItems?playlistId=UU_A--fhX5gea0i4UtpD99Gg&key=AIzaSyBiRp900Hz6RGVSxamdNEfBOA0TRfg3pK0&part=snippet&maxResults=50")!
        makeRequest(url: url, resultHandler: completion)
    }
    
    private func makeRequest(url: URL, resultHandler: @escaping YouTubeAPIResultHandler) {
        let dataTask = urlSession.dataTask(with: url) { (data, response, error) in
            
            if error != nil {
                resultHandler(nil, error!)
            }
            else {
                guard let sureData = data else {
                    resultHandler(nil, SWAPIError.badDataResponse)
                    return
                }
                
                do {
                    let json = try JSONSerialization.jsonObject(with: sureData, options: []) as? [String: Any]
                    
                    resultHandler(json, nil)
                }
                catch {
                    resultHandler(nil, SWAPIError.badDataResponse)
                }
                
            }
        }
        
        dataTask.resume()
    }
    
    deinit {
        urlSession.invalidateAndCancel()
    }
}
