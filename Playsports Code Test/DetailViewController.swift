//
//  DetailViewController.swift
//  Playsports Code Test
//
//  Created by Stephen Wilson on 02/05/2019.
//  Copyright © 2019 Stephen Wilson. All rights reserved.
//

import UIKit
import SDWebImage

class DetailViewController: UIViewController {

    
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var imageView: UIImageView!
    

    func configureView() {
        // Update the user interface for the detail item.
        if let detail = detailItem {
            
            if let snippet: [String:Any] = detail["snippet"] as? [String:Any] {
                var text = ""
                
                if let title = snippet["title"] as? String {
                    self.title = title
                    text += "\(title)\n\n"
                }
                
                if let publishedAtString = snippet["publishedAt"] as? String {
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                    dateFormatter.locale = NSLocale.current
                    
                    if let publishedAt = dateFormatter.date(from: publishedAtString) {
                        dateFormatter.dateFormat = "MMM d, h:mm a"
                        text += "Published: \(dateFormatter.string(from: publishedAt))\n\n"
                    }
                }
                
                // The video duration should be added in here but it requires a further API call.
                
                if let description = snippet["description"] as? String {
                    text += "\(description)"
                }
                
                self.textView?.text = text
                
                
                guard let thumbnails: [String:Any] = snippet["thumbnails"] as? [String:Any],
                    let imageDict: [String:Any] = thumbnails["high"] as? [String:Any],
                    let imageURLString = imageDict["url"] as? String,
                    let imageURL = URL(string: imageURLString) else {
                        return
                }
                
                SDWebImageManager.shared.loadImage(with: imageURL, options: .continueInBackground, progress: nil) { (image, data, error, cacheType, finished, url) in
                    self.imageView?.image = image
                }
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureView()
    }

    var detailItem: [String:Any]? {
        didSet {
            // Update the view.
            configureView()
        }
    }


}

